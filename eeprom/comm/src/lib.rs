#![no_std]

use desse::{Desse, DesseSized};

#[derive(Desse, DesseSized, Debug, Copy, Clone)]
pub enum Command {
    Ping(u8),
    Read { address: u16 },
    Write { address: u16, data: u8 },
}

#[derive(Desse, DesseSized, Debug, Copy, Clone)]
pub enum Response {
    Ping(u8),
    Read { data: u8 },
    Write,
}

#[derive(Desse, DesseSized, Debug, Copy, Clone)]
pub enum Data {
    Command(Command),
    Response(Response),
}

#[derive(Desse, DesseSized, Debug, Copy, Clone)]
pub struct Packet {
    pub id: u8,
    pub data: Data,
}
