use serde::{Serialize, Deserialize};

pub type Segments = Vec<Segment>;

#[derive(Serialize, Deserialize, Debug)]
pub struct Segment {
    pub label: String,
    pub offset: usize,
    pub data: Vec<u8>,
}
