use comm::*;
use desse::{Desse, DesseSized};
use embedded_hal::serial::{Read, Write};

pub trait WritePacket {
    fn write_packet(&mut self, packet: &Packet);
}

pub trait ReadPacket {
    fn try_read_packet(&mut self) -> Result<Option<Packet>, ()>;
}

/// Taken from the cobs crate and made const
const fn max_cobs_length(source_len: usize) -> usize {
    source_len + (source_len / 254) + if source_len % 254 > 0 { 1 } else { 0 }
}

impl<T> WritePacket for T
where
    T: Write<u8>,
{
    fn write_packet(&mut self, packet: &Packet) {
        // COBS encode the packet
        let mut cobs_buffer = [0u8; max_cobs_length(Packet::SIZE)];
        let length = cobs::encode(&packet.serialize(), &mut cobs_buffer);

        // write encoded data
        for byte in &cobs_buffer[..length] {
            nb::block!(self.write(*byte)).ok().unwrap();
        }

        // write packet end byte
        nb::block!(self.write(0x00)).ok().unwrap();
    }
}

impl<T> ReadPacket for T
where
    T: Read<u8>,
{
    fn try_read_packet(&mut self) -> Result<Option<Packet>, ()> {
        let mut cobs_buffer = [0u8; max_cobs_length(Packet::SIZE) + 1];
        let mut index = 0u8;

        // try reading a byte
        match self.read() {
            Ok(byte) => cobs_buffer[index as usize] = byte, // has data
            Err(nb::Error::WouldBlock) => return Ok(None),  // no data
            Err(nb::Error::Other(_)) => return Err(()),     // serial port failed
        }
        index += 1;

        // read until 0x00 byte or buffer is full
        while (index as usize) < cobs_buffer.len() {
            cobs_buffer[index as usize] = nb::block!(self.read()).map_err(|_| ())?;

            // check for the end of packet byte
            if cobs_buffer[index as usize] == 0x00 {
                // decode COBS data
                let length =
                    cobs::decode_in_place(&mut cobs_buffer[..index as usize]).map_err(|_| ())?;
                if length != Packet::SIZE {
                    return Err(()); // data is the wrong size
                }

                // deserialize packet
                let mut buffer = [0u8; Packet::SIZE];
                buffer.copy_from_slice(&cobs_buffer[..Packet::SIZE]);
                return Ok(Some(Packet::deserialize_from(&buffer)));
            }

            // move to next byte
            index += 1;
        }

        Err(()) // read a full buffer
    }
}
