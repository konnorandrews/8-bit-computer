use anyhow::Error;
use comm::Packet;
use desse::{DesseSized, Desse};

pub trait DesseVec: Sized {
    fn deserialize_from_slice(slice: &[u8]) -> Result<Self, Error>;
}

macro_rules! desse_vec {
    ($s:ident) => {
        impl DesseVec for $s {
            fn deserialize_from_slice(slice: &[u8]) -> Result<Self, Error> {
                let mut x = [0u8; Self::SIZE];
                if slice.len() < Self::SIZE {
                    return Err(Error::msg(format!(
                        "Slice is too small, expected {} bytes, got {} bytes",
                        Self::SIZE,
                        slice.len()
                    )));
                }
                x.copy_from_slice(&slice[..Self::SIZE]);
                Ok(Self::deserialize_from(&x))
            }
        }
    };
}

desse_vec!(Packet);
