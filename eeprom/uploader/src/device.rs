use anyhow::Error;
use serialport::SerialPort;
use std::{
    io::BufReader,
    thread,
    time::{Duration, Instant},
};
use comm::*;
use desse::Desse;
use crate::comm_vec::DesseVec;

pub fn wait_for_data(
    reader: &mut BufReader<Box<dyn SerialPort>>,
    timeout_sec: f64,
) -> Result<(), Error> {
    let start_time = Instant::now();
    while reader.get_ref().bytes_to_read().unwrap() == 0 {
        if Instant::now().duration_since(start_time).as_secs_f64() > timeout_sec {
            return Err(Error::msg("Programmer did not respond to command"));
        }
        thread::sleep(Duration::from_secs_f64(0.001));
    }

    Ok(())
}

pub fn read_packet(reader: &mut BufReader<Box<dyn SerialPort>>) -> Result<Packet, Error> {
    use std::io::BufRead;

    let mut buffer = vec![];
    reader.read_until(0x00, &mut buffer)?;
    let buffer_len = buffer.len();
    let cobs_response_slice = &mut buffer[..buffer_len - 1];
    let length = cobs::decode_in_place(cobs_response_slice)
        .map_err(|_| Error::msg("Failed to decode serial packet"))?;
    let response_slice = &buffer[..1 + length];
    Ok(Packet::deserialize_from_slice(response_slice)?)
}

pub fn wait_for_startup(reader: &mut BufReader<Box<dyn SerialPort>>) -> Result<(), Error> {
    wait_for_data(reader, 5.0)?;

    match read_packet(reader)? {
        Packet {
            id: 42,
            data: Data::Response(Response::Ping(101)),
        } => Ok(()),
        _ => Err(Error::msg("Programmer not recognized")),
    }
}

pub fn device_request(
    command: &Command,
    reader: &mut BufReader<Box<dyn SerialPort>>,
) -> Result<Response, Error> {
    use std::io::Write;

    let packet_id = rand::random();
    let packet = Packet {
        id: packet_id,
        data: Data::Command(command.clone()),
    };

    let mut buffer = cobs::encode_vec(&packet.serialize());
    buffer.push(0);
    reader.get_mut().write_all(&buffer)?;

    wait_for_data(reader, 1.0)?;

    match read_packet(reader)? {
        Packet {
            id,
            data: Data::Response(response),
        } if id == packet_id => Ok(response),
        _ => Err(Error::msg("Programmer sent an invalid response")),
    }
}
