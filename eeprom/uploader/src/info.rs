#[macro_export]
macro_rules! task_message {
    ($verb:expr, $($fmt:tt)+) => {
        eprint!("{:>12} ", colored::Colorize::bold(colored::Colorize::green($verb)));
        eprintln!($($fmt)+);
    };
}

pub fn print_error(e: anyhow::Error) {
    use colored::Colorize as _;

    eprintln!("{:>12} {}", "Error".red().bold(), e.to_string());

    for cause in e.chain().skip(1) {
        eprintln!("{:>12} {}", "Caused by".yellow().bold(), cause.to_string());
    }
}
