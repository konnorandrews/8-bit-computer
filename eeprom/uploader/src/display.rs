pub fn display_buffer(data: Vec<u8>, offset: usize) {
    let line_offset = offset % 16;
    let num_lines = ((line_offset + data.len()) as f32 / 16.0).ceil() as usize;

    {
        let mut header = "".to_string();
        for index in 0..16 {
            if index == 8 {
                header += " ";
            }

            header += &format!(" {:2X}", index);
        }
        println!("{:4} {}", "", header);
    }

    for line in 0..num_lines {
        let mut buf = "".to_string();
        for index in 0..16 {
            if index == 8 {
                buf += " ";
            }

            let data_index = (line * 16 + index).wrapping_sub(line_offset);
            if let Some(byte) = data.get(data_index) {
                buf += &format!(" {:02X}", byte);
            } else {
                buf += " ??";
            }
        }
        println!("{:04X} {}", line * 16 + offset - line_offset, buf);
    }
}
