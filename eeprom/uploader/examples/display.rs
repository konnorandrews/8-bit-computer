use image::*;

const DIGITS: [u8; 18] = [
    0x3F, // 0
    0x0C, // 1
    0x5B, // 2
    0x5E, // 3
    0x6C, // 4
    0x76, // 5
    0x77, // 6
    0x1C, // 7
    0x7F, // 8
    0x7E, // 9
    0x7D, // A
    0x67, // B
    0x43, // C
    0x4F, // D
    0x73, // E
    0x71, // F
    0x40, // -
    0x00, // _
];

fn main() {
    let image = vec![
        Segment {
            label: "unsigned".to_string(),
            offset: 0,
            data: gen_unsigned(),
        },
        Segment {
            label: "signed".to_string(),
            offset: 1024,
            data: gen_signed(),
        },
    ];
    std::fs::write("display.image", serde_json::to_string(&image).unwrap()).unwrap();
}

fn gen_unsigned() -> Vec<u8> {
    let mut buf = vec![0u8; 256*4];
    for i in 0..=255u8 {
        let n = i;
        let s = format!("{:>4}", n.to_string());
        buf[0 * 256usize + (i as usize)] = get_code(s.chars().nth(0).unwrap());
        buf[1 * 256usize + (i as usize)] = get_code(s.chars().nth(1).unwrap());
        buf[2 * 256usize + (i as usize)] = get_code(s.chars().nth(2).unwrap());
        buf[3 * 256usize + (i as usize)] = get_code(s.chars().nth(3).unwrap());
    }
    println!("{:?}", buf);
    buf
}

fn gen_signed() -> Vec<u8> {
    let mut buf = vec![0u8; 256*4];
    for i in 0..=255u8 {
        let n = i as i8;
        let s = format!("{:>4}", n.to_string());
        buf[0 * 256usize + (i as usize)] = get_code(s.chars().nth(0).unwrap());
        buf[1 * 256usize + (i as usize)] = get_code(s.chars().nth(1).unwrap());
        buf[2 * 256usize + (i as usize)] = get_code(s.chars().nth(2).unwrap());
        buf[3 * 256usize + (i as usize)] = get_code(s.chars().nth(3).unwrap());
    }
    println!("{:?}", buf);
    buf
}

fn get_code(c: char) -> u8 {
    match c {
        '0' => DIGITS[0],
        '1' => DIGITS[1],
        '2' => DIGITS[2],
        '3' => DIGITS[3],
        '4' => DIGITS[4],
        '5' => DIGITS[5],
        '6' => DIGITS[6],
        '7' => DIGITS[7],
        '8' => DIGITS[8],
        '9' => DIGITS[9],
        '-' => DIGITS[16],
        ' ' => DIGITS[17],
        _ => panic!(),
    }
}
