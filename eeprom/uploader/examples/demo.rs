use std::fs;
use std::io::Write;

use image::*;
use zip::write::FileOptions;
use zip::ZipWriter;

fn main() {
    let image = vec![Segment {
        label: "test".to_string(),
        offset: 0x16,
        data: vec![0x01, 0x02, 0x03],
    }];
    std::fs::write("test.image", serde_json::to_string(&image).unwrap()).unwrap();
}
