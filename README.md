# 8-bit Computer

This project contains support software for [Ben Eater's 8-bit computer kit](https://eater.net/8bit/) written in Rust. I am creating these tools while putting together the kit. 

## Tools

The following tools are currently available.

- EEPROM Programmer
  - Programmer firmware - Firmware for an Atmega328P chip
  - Image uploader - Tool to upload EEPROM image to programmer
